const withAnalyzer = require("@next/bundle-analyzer")({
  enabled: process.env.ANALYZE === "true",
});
const withSass = require("@zeit/next-sass");
const TsConfigPathsPlugin = require("tsconfig-paths-webpack-plugin");
const withTM = require("next-transpile-modules")([
  // Those packages needs to be transpiled to be sure they pass `es-check es5`
  "react-id-swiper",
  "swiper",
  "dom7",
]);
const FilterWarningsPlugin = require("webpack-filter-warnings-plugin");

const config = withAnalyzer(
  withSass(
    withTM({
      webpack(config) {
        // adds support for typescript's paths
        const plugin = new TsConfigPathsPlugin({
          configFile: "./src/tsconfig.json",
        });
        config.resolve.plugins = config.resolve.plugins || [];
        config.resolve.plugins.push(plugin);
        config.plugins.push(
          new FilterWarningsPlugin({
            exclude: /mini-css-extract-plugin[^]*Conflicting order between:/,
          })
        );

        return config;
      },
    })
  )
);

config.env = {
  ENV: process.env.ENV,
};

config.experimental = {
  polyfillsOptimization: true,
};

module.exports = config;
