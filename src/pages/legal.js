import "./index.scss";
import LegalPage from "../components/LegalPage.js";
import Layout from "../components/Layout.js";

export default () => (
  <div className="main">
    <Layout title="Zwolf - Design & Tech Prototyping Agency">
      <LegalPage />
    </Layout>
  </div>
);
