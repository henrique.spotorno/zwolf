import "./index.scss";
import IndexPageCard from "../components/indexPages/IndexPageCard.js";
import Layout from "../components/Layout.js";
import IndexPageStartup from "../components/indexPages/IndexPageStartup";
import IndexPagePoster from "../components/indexPages/IndexPagePoster";

export default () => {
  const randomPageNumber = Math.random();
  let randomPage = <div />;
  switch (true) {
    case 0 <= randomPageNumber && randomPageNumber <= 0.00001:
      randomPage = <IndexPagePoster />;
      break;
    default:
      randomPage = <IndexPageStartup />;
  }

  return (
    <div className="main">
      <Layout title="Zwolf - Design & Tech Prototyping Agency">
        {randomPage}
      </Layout>
    </div>
  );
};
