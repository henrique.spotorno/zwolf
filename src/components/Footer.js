import "./Footer.scss";

const Footer = () => (
  <div className="w-full bg-dark">
    <img src="/static/zwolf_logo_white.svg" className="footer-img" />
  </div>
);

export default Footer;
