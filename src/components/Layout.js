import Head from "next/head";

import "./Layout.scss";

function Layout(props) {
  const { children, title } = props;
  return (
    <div>
      <Head>
        <meta charSet="utf-8" />
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <meta
          name="description"
          content="Zwolf brings together design, technology, innovation, and business development to deliver best-of-class MVPs and prototypes."
        />
        <meta property="og:title" content="Zwolf - Design & Tech" />
        <meta
          property="og:description"
          content="Bringing together design, technology, innovation, and your ideas."
        />
        <meta property="og:image" content="/static/zwolf.png" />
        <meta property="og:url" content="https://zwolf.io" />
        <meta name="twitter:card" content="Zwolf - Design & Tech" />
        <meta property="og:site_name" content="Zwolf" />
        <meta name="twitter:image:alt" content="Zwolf - Design & Tech" />

        <link
          rel="shortcut icon"
          href="/static/favicon.png"
          type="image/x-icon"
        />
        <link rel="icon" href="/static/favicon.png" type="image/x-icon" />
        <link rel="stylesheet" href="https://use.typekit.net/vae6vla.css" />

        <title>{title}</title>
      </Head>
      <div className="">
        <div className="content-group">
          <div className="content">{children}</div>
        </div>
      </div>
    </div>
  );
}

export default Layout;
