import "./LegalPage.scss";

const LegalPage = () => (
  <div className="container">
    <div className="row bordered-body">
      <div className="body-full">
        <span>Firma: Unsourced Digital OG</span>
        <span>Email: contact@unsourced-digital.com</span>
        <span>UID: ATU73520969</span>
        <span>Firmenbuchnummer: FN 494892 h</span>
        <span>Firmenbuchgericht: Handelsgericht Wien</span>
        <span>
          Behörde gem. ECG: Magistratisches Bezirksamt des IX. Bezirkes
        </span>
        <span>Unternehmensgegenstand: IT-Dienstleistung</span>
        <span>Kammer: WKO Wien</span>
        <span>Beginndatum der Rechtsform: Juni 2018</span>
        <a href="/">Back</a>
      </div>
    </div>
  </div>
);

export default LegalPage;
