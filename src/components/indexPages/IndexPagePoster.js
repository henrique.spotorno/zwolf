import "./IndexPagePoster.scss";
import SocialLinks from "../common/SocialLinks";
import Footer from "../Footer";

const IndexPagePoster = () => (
  <div className="flex flex-col poster-screen">
    <div
      id="section-0"
      className="w-screen min-h-screen flex flex-col bg-dark-poster"
    >
      <div className="flex flex-1 page-spacing">
        <div className="w-full sm:w-1/2 h-screen sm:h-full flex flex-col">
          <div className="m-auto">
            <div className="text-title text-white text-center sm:text-left mt-6 mb-2 sm:mb-12 font-p22">
              We do creative development services for{" "}
              <span className="underline">fun</span> and{" "}
              <span className="underline">money</span>.
            </div>
            <div className="flex sm:hidden mb-2">
              <div className="m-auto h-main-image">
                <img className="h-inherit" src="/static/wolfy-2.svg" />
              </div>
            </div>
            <SocialLinks darkBg={true} />
          </div>
        </div>
        <div className="sm:w-1/2 hidden sm:flex flex-col">
          <div className="my-auto ml-auto flex flex-col">
            <img
              className="main-image h-inherit"
              src="/static/henrique-suit.jpg"
            />
            <span className="text-white pt-3 text-right ml-auto">
              12 unique postcards not included.
            </span>
          </div>
        </div>
      </div>
      <div className="flex bg-yellow-startup w-full height-bar mt-auto">
        <div className="ml-auto my-auto size-logo position-logo">
          <img src="/static/zwolf_logo.png" />
        </div>
      </div>
    </div>
    <div id="section-1" className="w-screen h-section flex flex-col bg-blue">
      <div className="flex flex-1">
        <div className="w-full flex flex-col">
          <div className="w-full sm:w-10/12 my-auto page-spacing-left">
            <div className="text-title text-white text-center sm:text-left mb-8 sm:mb-12 font-p22">
              We <span className="underline">design</span> &{" "}
              <span className="underline">develop</span> websites and
              applications.
            </div>
            <div className="text-white text-subtitle text-center sm:text-left">
              Have an idea? Contact us now!
              <div>
                <a className="text-white" href="mailto:bark@zwolf.io">
                  bark@zwolf.com
                </a>
              </div>
            </div>
            <div className="vertical-text">Based in Vienna, Austria</div>
          </div>
        </div>
      </div>
    </div>
    <div
      id="section-2"
      className="w-screen min-h-screen flex flex-col bg-gray-startup"
    >
      <div className="flex flex-1">
        <div className="w-full flex">
          <div className="w-full sm:w-1/2 my-auto page-spacing flex flex-col">
            <div className="text-title text-center sm:text-left my-6 sm:mb-12 font-p22">
              Our <span className="underline">Lovely</span> Customers
              <img className="heart-img" src="/static/heart.svg" />
            </div>
            <div className="flex flex-col">
              <div className="text-subtitle text-center sm:text-left">
                “They are so cool, and added so much flavour that we could not
                be happier.”
              </div>
              <div className="text-center sm:text-left text-gray leading-tight my-6">
                <div className="text-lg">Marko Haschej</div>
                <div className="font-light text-sm">CEO & Founder</div>
              </div>
              <div className="mx-auto sm:mx-0">
                <img className="w-24 sm:w-32" src="/static/mp-logo.svg" />
              </div>
            </div>
            <div className="flex sm:hidden">
              <img className="client-img" src="/static/moonshot-pirates.png" />
            </div>
          </div>
          <div className="sm:w-1/2 hidden sm:flex">
            <div className="m-auto h-main-image">
              <img className="client-img" src="/static/moonshot-pirates.png" />
            </div>
          </div>
        </div>
      </div>
    </div>
    <Footer />
  </div>
);

export default IndexPagePoster;
