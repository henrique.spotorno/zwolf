import "./IndexPageCard.scss";
import Card from "../common/Card";

const IndexPageCard = () => (
  <div className="index-container website-border">
    <div>
      <div>
        <div className="umlaut-ears">
          <img src="static/pfad1.svg" className="first-ear" />
          <img src="static/pfad2.svg" className="second-ear" />
        </div>
        <h1>ZWOLF</h1>
        <h2>DESIGN & TECH</h2>
        <p>We turn your idea into your product.</p>
      </div>
      <div className="flex">
        <div className="wolf-card mx-auto">
          <Card />
        </div>
      </div>
      <div>
        CREATIVE DESIGN AGENCY COLLECTIVE QUANTUM-ENHANCED SOY-BASED STUDIO
        GLUTEN-FREE GROUP *
      </div>
    </div>
  </div>
);

export default IndexPageCard;
