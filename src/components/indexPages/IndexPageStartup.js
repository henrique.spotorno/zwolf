import "./IndexPageStartup.scss";
import SocialLinks from "../common/SocialLinks";
import Footer from "../Footer";

const IndexPageStartup = () => (
  <div className="flex flex-col startup-screen">
    <div
      id="section-0"
      className="w-screen min-h-screen flex flex-col bg-gray-startup"
    >
      <div className="flex flex-1 page-spacing">
        <div className="w-full sm:w-1/2 h-full min-h-80vh flex flex-col">
          <div className="m-auto h-full flex flex-col">
            <div className="text-title text-center sm:text-left mt-6 sm:mt-10 md:mt-12 xl:mt-16 mb-2 sm:mb-12 font-p22">
              We do creative development services for{" "}
              <span className="underline">fun</span> and{" "}
              <span className="underline">money</span>.
            </div>
            <div className="flex sm:hidden mb-2">
              <div className="m-auto h-main-image">
                <img className="h-inherit" src="/static/wolfy-2.svg" />
              </div>
            </div>
            <SocialLinks darkBg={false} />
          </div>
        </div>
        <div className="sm:w-1/2 hidden sm:flex">
          <div className="my-auto ml-auto h-main-image">
            <img className="h-inherit" src="/static/wolfy.svg" />
          </div>
        </div>
      </div>
      <div className="flex bg-yellow-startup w-full height-bar mt-auto">
        <div className="ml-auto my-auto size-logo position-logo">
          <img src="/static/zwolf_logo.png" />
        </div>
      </div>
    </div>
    <div
      id="section-1"
      className="w-screen h-section flex flex-col bg-yes-its-png-sue-me"
    >
      <div className="flex flex-1">
        <div className="w-full flex flex-col">
          <div className="w-full sm:w-10/12 my-auto page-spacing-left">
            <div className="text-title text-white text-center sm:text-left mb-8 sm:mb-12 font-p22">
              We <span className="underline">design</span> &{" "}
              <span className="underline">develop</span> websites and
              applications.
            </div>
            <div className="text-white text-subtitle text-center sm:text-left">
              Have an idea? Contact us now!
              <div>
                <a className="text-white" href="mailto:bark@zwolf.io">
                  bark@zwolf.com
                </a>
              </div>
            </div>
            <div className="vertical-text">Based in Vienna, Austria</div>
          </div>
        </div>
      </div>
    </div>
    <div
      id="section-2"
      className="w-screen min-h-screen flex flex-col bg-gray-startup"
    >
      <div className="flex flex-1">
        <div className="w-full flex">
          <div className="w-full sm:w-1/2 my-auto page-spacing flex flex-col">
            <div className="text-title text-center sm:text-left my-6 sm:mb-12 font-p22">
              Our <span className="underline">Lovely</span> Customers
              <img className="heart-img" src="/static/heart.svg" />
            </div>
            <div className="flex flex-col">
              <div className="text-subtitle text-center sm:text-left">
                “These guys are amazing. They create epic shit, they celebrate
                their milestones properly, but most importantly they care and
                will give everything to make things work!”
              </div>
              <div className="text-center sm:text-left text-gray leading-tight my-6">
                <div className="text-lg">Marko Haschej</div>
                <div className="font-light text-sm">
                  Chief-Changemaker & Co-Founder
                </div>
              </div>
              <div className="mx-auto sm:mx-0">
                <img className="w-24 sm:w-32" src="/static/mp-logo.svg" />
              </div>
            </div>
            <div className="flex sm:hidden">
              <div className="m-auto h-main-image client-img-container">
                <img
                  className="client-img"
                  src="/static/moonshot-pirates.jpg"
                />
              </div>
            </div>
          </div>
          <div className="sm:w-1/2 hidden sm:flex">
            <div className="m-auto h-main-image client-img-container">
              <img className="client-img" src="/static/moonshot-pirates.jpg" />
            </div>
          </div>
        </div>
      </div>
    </div>
    <Footer />
  </div>
);

export default IndexPageStartup;
