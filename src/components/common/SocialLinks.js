import "./SocialLinks.scss";

const SocialLinks = (props) => {
  const { darkBg } = props;
  return (
    <div className="flex pb-4 mx-auto sm:mx-0">
      <div className="w-circle-icons flex flex-col mr-2 sm:ml-0 sm:mr-12">
        <div
          style={
            darkBg
              ? {
                  backgroundColor: "#fff",
                  color: "#1a1818",
                  border: "4px #1a1818 solid",
                }
              : { backgroundColor: "#1a1818", color: "#fff" }
          }
          className="circle-icons mx-auto my-2"
        >
          <a href="https://dribbble.com/FerreraPaulo" target="_blank">
            <svg
              style={darkBg ? { color: "#1a1818" } : { color: "#fff" }}
              className="work-icons"
              xmlns="http://www.w3.org/2000/svg"
              width="39"
              height="39"
              viewBox="0 0 39 39"
              fill="currentColor"
            >
              <path d="M0,19.5A19.1,19.1,0,0,1,2.613,9.711a19.362,19.362,0,0,1,7.1-7.1A19.1,19.1,0,0,1,19.5,0a19.1,19.1,0,0,1,9.789,2.613,19.361,19.361,0,0,1,7.1,7.1A19.1,19.1,0,0,1,39,19.5a19.1,19.1,0,0,1-2.613,9.789,19.361,19.361,0,0,1-7.1,7.1A19.1,19.1,0,0,1,19.5,39a19.1,19.1,0,0,1-9.789-2.613,19.362,19.362,0,0,1-7.1-7.1A19.1,19.1,0,0,1,0,19.5Zm3.237,0a15.725,15.725,0,0,0,4.1,10.725,22.318,22.318,0,0,1,5.928-6.981,19.388,19.388,0,0,1,7.917-4.173q-.585-1.365-1.131-2.457A47.386,47.386,0,0,1,5.538,18.759q-1.521,0-2.262-.039,0,.156-.02.39T3.237,19.5Zm.507-4.017q.858.078,2.535.078a42.8,42.8,0,0,0,12.363-1.755,39.49,39.49,0,0,0-6.513-8.775A15.832,15.832,0,0,0,6.845,9.36,16.651,16.651,0,0,0,3.744,15.483ZM9.555,32.331a15.937,15.937,0,0,0,15.678,2.34,57.267,57.267,0,0,0-3.042-12.909A16.633,16.633,0,0,0,14.957,25.7,22.123,22.123,0,0,0,9.555,32.331ZM15.522,3.783a41.445,41.445,0,0,1,6.357,8.853,19.423,19.423,0,0,0,8-5.655A15.809,15.809,0,0,0,19.5,3.237,14.97,14.97,0,0,0,15.522,3.783Zm7.761,11.622q.585,1.248,1.326,3.159,2.886-.273,6.279-.273,2.418,0,4.8.117A15.658,15.658,0,0,0,31.863,8.97Q29.328,12.753,23.283,15.405Zm2.3,5.889A55.978,55.978,0,0,1,28.275,33.15a16.272,16.272,0,0,0,5.031-5.109,15.752,15.752,0,0,0,2.34-6.747Q32.8,21.1,30.459,21.1,28.314,21.1,25.584,21.294Z" />
            </svg>
          </a>
        </div>
        <div
          className="text-center leading-none"
          style={darkBg ? { color: "#fff" } : { color: "#1a1818" }}
        >
          Visual Works
        </div>
      </div>
      <div className="w-circle-icons flex flex-col mx-2 sm:ml-0 sm:mr-12">
        <div
          style={
            darkBg
              ? {
                  backgroundColor: "#fff",
                  color: "#1a1818",
                  border: "4px #1a1818 solid",
                }
              : { backgroundColor: "#1a1818", color: "#fff" }
          }
          className="circle-icons mx-auto my-2"
        >
          <a href="https://github.com/Mytrill" target="_blank">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="39"
              height="38.038"
              viewBox="0 0 39 38.038"
              fill="currentColor"
              className="work-icons"
              style={
                darkBg
                  ? { color: "#1a1818", fillRule: "evenodd" }
                  : { color: "#fff", fillRule: "evenodd" }
              }
            >
              <path d="M19.5,0a19.5,19.5,0,0,0-6.167,38c.975.171,1.341-.414,1.341-.926,0-.463-.024-2-.024-3.632-4.9.9-6.167-1.194-6.557-2.291a7.091,7.091,0,0,0-2-2.754c-.682-.366-1.657-1.267-.024-1.292,1.536-.024,2.633,1.414,3,2a4.168,4.168,0,0,0,5.679,1.609A4.1,4.1,0,0,1,15.99,28.1c-4.339-.487-8.872-2.169-8.872-9.628a7.589,7.589,0,0,1,2-5.241,7.007,7.007,0,0,1,.195-5.167s1.633-.512,5.363,2a18.385,18.385,0,0,1,9.75,0c3.729-2.535,5.363-2,5.363-2a7.007,7.007,0,0,1,.195,5.167,7.545,7.545,0,0,1,2,5.241c0,7.483-4.558,9.141-8.9,9.628A4.618,4.618,0,0,1,24.4,31.712c0,2.608-.024,4.7-.024,5.362,0,.512.366,1.121,1.341.926A19.51,19.51,0,0,0,19.5,0Z" />
            </svg>
          </a>
        </div>
        <div
          className="text-center leading-none"
          style={darkBg ? { color: "#fff" } : { color: "#1a1818" }}
        >
          Technically Correct
        </div>
      </div>
      <div className="w-circle-icons text-center flex flex-col mx-2">
        <div
          style={
            darkBg
              ? {
                  backgroundColor: "#fff",
                  color: "#1a1818",
                  border: "4px #1a1818 solid",
                }
              : { backgroundColor: "#1a1818", color: "#fff" }
          }
          className="circle-icons mx-auto my-2"
        >
          <a href="https://medium.com/freeler" target="_blank">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="work-icons"
              width="34.731"
              height="27.496"
              viewBox="0 0 34.731 27.496"
              style={darkBg ? { color: "#1a1818" } : { color: "#fff" }}
            >
              <path
                fill="currentColor"
                d="M34.731,4.052H33.358a1.559,1.559,0,0,0-1.231,1.207V22.329a1.458,1.458,0,0,0,1.231,1.114h1.374V27.5H22.286V23.444h2.6V5.5h-.128l-6.083,22H13.971l-6-22H7.815V23.444h2.6V27.5H0V23.444H1.334A1.46,1.46,0,0,0,2.6,22.329V5.259A1.556,1.556,0,0,0,1.334,4.052H0V0H13.029l4.278,15.919h.118L21.741,0h12.99V4.052"
              />
            </svg>
          </a>
        </div>
        <div
          className="text-center leading-none"
          style={darkBg ? { color: "#fff" } : { color: "#1a1818" }}
        >
          Conspiracy Theories
        </div>
      </div>
    </div>
  );
};

export default SocialLinks;
