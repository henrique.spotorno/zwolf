import "./Shadow.scss";

function Shadow(props) {
  const { children } = props;
  return (
    <div className="background-shadow h-inherit w-inherit">{children}</div>
  );
}
export default Shadow;
