import Shadow from "./Shadow";
import "./Card.scss";

const Card = () => (
  <div className="h-inherit w-inherit">
    <Shadow>
      <div className="card-container">
        <div className="card">12</div>
      </div>
    </Shadow>
  </div>
);

export default Card;
